TOPDIR=/tmp/rpmbuild
rm -rf $TOPDIR
rm -f *.rpm
mkdir -p $TOPDIR/SOURCES

tar czf $TOPDIR/SOURCES/mdtest-1.9.3.tar.gz mdtest-1.9.3
rpmbuild --define "_topdir $TOPDIR" -bs mdtest.spec

cp $TOPDIR/SRPMS/*.src.rpm .
rpmbuild --define "_topdir $TOPDIR" --define "_bindir /usr/local/bin/" --rebuild *.src.rpm
cp $TOPDIR/RPMS/*/* .