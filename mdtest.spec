#
# spec file for package mdtest
#
# Copyright (c) 2016 SUSE LINUX GmbH, Nuernberg, Germany.
#
# All modifications and additions to the file contributed by third parties
# remain the property of their copyright owners, unless otherwise agreed
# upon. The license for this file, and modifications and additions to the
# file, is the same license as for the pristine package itself (unless the
# license for the pristine package is not an Open Source License, in which
# case the license is the MIT License). An "Open Source License" is a
# license that conforms to the Open Source Definition (Version 1.9)
# published by the Open Source Initiative.

# Please submit bugfixes or comments via http://bugs.opensuse.org/
#


Name:           mdtest
Version:        1.9.3
Release:        4.2%{?dist}
Summary:        An MPI-coordinated test that performs operations on files and directories
License:        GPL-2.0
Group:          System/Benchmark
Url:            http://mdtest.sourceforge.net/
Source:         http://sourceforge.net/projects/mdtest/files/mdtest%%20latest/%{name}-%{version}/%{name}-%{version}.tar.gz
BuildRequires:  mpich-devel
Requires: mpich
BuildRoot:      %{_tmppath}/%{name}-%{version}-build

%description
mdtest is an MPI-coordinated metadata benchmark test that performs
open/stat/close operations on files and directories and then reports the
performance.

%prep
%setup -q
chmod -x RELEASE_LOG README COPYRIGHT

%build
%{?_mpich_load}
make %{?_smp_mflags} MDTEST_FLAGS="%{optflags}" MPI_CC="mpicc"
%{?_mpich_unload}

%install
%{?_mpich_load}

%if 0%{?rhel} != 8
MPI_BIN=%{_bindir}
%endif

install -D -p -m 0755 mdtest   %{buildroot}${MPI_BIN}/mdtest
install -D -p -m 0644 mdtest.1 %{buildroot}%{_mandir}/man1/mdtest.1
%{?_mpich_unload}

%files
%defattr(-,root,root)
%doc RELEASE_LOG README COPYRIGHT

%if 0%{?rhel} == 8
%{_libdir}/mpich/bin/mdtest
%else
%{_bindir}/mdtest
%endif

%{_mandir}/man1/mdtest.1.gz

%changelog
* Sat Aug 4 2018 c17454@cray.com
- use mpich instead of openmpi
* Sat Jul  2 2016 mpluskal@suse.com
- Use mpicc from openmpi
- Build with optflags
* Thu Apr 16 2015 mpluskal@suse.com
- Update url
* Thu Apr 16 2015 jhura@suse.com
- initial package for version 1.9.3
