#!/bin/bash

export MPI_CC=mpicc
cd mdtest-*
checkinstall --install=no --pkgname=mdtest -y -D sh -c "make && cp mdtest /usr/local/bin"
